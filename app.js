import Koppa from './core/index.js';

export default (async function(that = {}) {
  await Koppa(that);

  await that.renderer.digest({
    url: 'SideNav',
    ref: document.body,
    type: 'component'
  });
})();
