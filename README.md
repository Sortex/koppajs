# Koppa

**Koppa.js** is a client-side rendering framework for single page applications which gets along without virtual DOM.

## Overview
Here is a small overview what Koppa.js offers.
1. Pages, Components (single File Templates)
2. JS-Module (extend the functionality)
3. Router
4. templating
5. live data

and many more...

### Installing

The .htaccess file only needed if koppa.js is running on an Apache server.

If you use an nginx you should set the vhost config rules like the .htaccess file.

## More
**Koppa.js** is also available on **Node.js** [here](https://www.npmjs.com/package/@sortex/koppa).
